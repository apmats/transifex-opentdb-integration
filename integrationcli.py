import json
import requests
import pprint
import argparse
import hashlib
import transifexutils
import opentdbutils
import integration


def main():
    ap = argparse.ArgumentParser(
        description="Will be fetching questions for the categories provided as command line arguments")
    ap.add_argument("-c", "--categories", nargs="+",
                    help="Any number of categories")
    ap.add_argument("-n", "--num", type=int, default=10,
                    help="Questions per category. Defaults to 10")
    ap.add_argument("-t", "--token", required=True, 
                    help="An API token to be used for the transifex requests")
    ap.add_argument("-p", "--project", required=True, 
                    help="An existing project under the transifex account to upload the question data to. If it doesn't exist it will be created")
    args = ap.parse_args()

    integration.fetchAndUploadQuestions(
        args.categories, args.num, args.token, args.project)


if __name__ == "__main__":
    main()
