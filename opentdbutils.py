import requests
from question import Question

# url and parameter name  constants
OPENTDB_BASE_URL = "https://opentdb.com"
OPENTDB_API_SUBPATH = "api.php"
OPENTDB_CATEGORY_API_SUBPATH = "api_category.php"
OPENTDB_QUESTION_NUMBER_PARAM = "amount"
OPENTDB_CATEGORY_PARAM = "category"
OPENTDB_CATEGORY_API_PATH = "/".join([OPENTDB_BASE_URL, OPENTDB_CATEGORY_API_SUBPATH])
OPENTDB_API_PATH = "/".join([OPENTDB_BASE_URL, OPENTDB_API_SUBPATH])


def getAllCategories():
    try:
        respJson = requests.get(OPENTDB_CATEGORY_API_PATH).json()
        allCategories = respJson["trivia_categories"]
        categoryNameToId = {}
        for category in allCategories:
            categoryNameToId[category["name"]] = str(category["id"])
        return categoryNameToId
    except:
        raise Exception("Failed to retrieve available categories")


def getQuestionsForCategory(category, numOfQuestions):
    questions = []
    try:
        categoryResp = requests.get(OPENTDB_API_PATH, params={
                                    OPENTDB_QUESTION_NUMBER_PARAM: str(numOfQuestions), OPENTDB_CATEGORY_PARAM: category})
        for result in categoryResp.json()["results"]:
            question = Question(
                result['question'], result['correct_answer'], result['incorrect_answers'])
            questions.append(question)
        return questions
    except:
        raise Exception("Failed to retrieve questions from opentdb api")
