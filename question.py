# Simple dto that helps to define the contract between our 2 separate modules and acheive separation of concerns
class Question:
    def __init__(self, question, correctAnswer, incorrectAnswers):
        self.question = question
        self.correctAnswer = correctAnswer
        self.incorrectAnswers = incorrectAnswers

    def __eq__(self, obj):
        return (isinstance(obj, Question) and
                obj.question == self.question and
                obj.correctAnswer == self.correctAnswer and
                obj.incorrectAnswers == self.incorrectAnswers)
