import json
import requests
import hashlib

# url and other constants
TRANSIFEX_BASE_URL = "https://www.transifex.com"
TRANSIFEX_API_PATH = "api/2"
TRANSIFEX_PROJECT_SUBPATH = "project"
TRANSIFEX_RESOURCE_SUBPATH = "resource"
TRANSIFEX_RESOURCES_SUBPATH = "resources"
TRANSIFEX_CONTENT_SUBPATH = "content"

TRANSIFEX_PROJECT_PATH = "/".join([TRANSIFEX_BASE_URL,
                                   TRANSIFEX_API_PATH, TRANSIFEX_PROJECT_SUBPATH, "{}"])
TRANSIFEX_RESOURCE_PATH = "/".join([TRANSIFEX_PROJECT_PATH,
                                    TRANSIFEX_RESOURCE_SUBPATH, "{}"])
TRANSIFEX_CONTENT_PATH = "/".join([TRANSIFEX_RESOURCE_PATH,
                                   TRANSIFEX_CONTENT_SUBPATH])
TRANSIFEX_RESOURCES_PATH = "/".join([TRANSIFEX_PROJECT_PATH,
                                     TRANSIFEX_RESOURCES_SUBPATH])

TRANSIFEX_API_USER = "api"


def uploadQuestions(questions, category, project, authToken):
    newData = _transformQuestionsIntoContentFormat(questions)
    existingData = _getCurrentCategoryContent(project, category, authToken)
    if not existingData:
        _createCategoryResourceAndSendContent(
            project, category, authToken, newData)
    else:
        existingData.update(newData)
        _sendCategoryContentToExistingResource(
            project, category, authToken, existingData)


def _transformQuestionsIntoContentFormat(questions):
    res = {}
    for question in questions:
        question_hash = hashlib.md5(
            question.question.encode('utf-8')).hexdigest()
        res[question_hash + "_question"] = question.question
        res[question_hash + "_correct_answer"] = question.correctAnswer
        for i, incorrectAnswer in enumerate(question.incorrectAnswers):
            res[question_hash+"_incorrect_answer"+str(i)] = incorrectAnswer
    return res


def _parseContentResp(contentResp):
    return json.loads(contentResp.json()['content'])


def _getCurrentCategoryContent(project, category, authToken):
    reqUrl = TRANSIFEX_CONTENT_PATH.format(project, category)
    contentResp = requests.get(reqUrl, auth=(TRANSIFEX_API_USER, authToken))
    # Failure here is silent, even if this happens due to an error,
    # such as invalid project or token.
    # This is fine, we get this check for free
    # since we would check for resource existence anyway.
    # An exception is raised down the line when 
    # resource creations fails for the same reason.
    if contentResp.status_code != requests.codes.ok:
        return {}
    return _parseContentResp(contentResp)


def _sendCategoryContentToExistingResource(project, category, authToken, categoryData):
    headers, data = _setupDataAndHeaders(category, categoryData)
    reqUrl = TRANSIFEX_CONTENT_PATH.format(project, category)
    resp = requests.put(reqUrl, headers=headers, data=json.dumps(
        data), auth=(TRANSIFEX_API_USER, authToken))
    if resp.status_code != requests.codes.ok:
        raise Exception(
            "Failed to upload content to existing resource for category: " + category + " , make sure that the provided api key is correct")


def _createCategoryResourceAndSendContent(project, category, authToken, categoryData):
    reqUrl = TRANSIFEX_RESOURCES_PATH.format(project)
    headers, data = _setupDataAndHeaders(category, categoryData)
    data["i18n_type"] = "KEYVALUEJSON"
    resp = requests.post(reqUrl, headers=headers, data=json.dumps(
        data), auth=(TRANSIFEX_API_USER, authToken))
    if resp.status_code != requests.codes.created:
        raise Exception(
            "Failed to upload content to new resource for category: " + category + " , make sure that the project exists and the provided api key is correct")


def _setupDataAndHeaders(category, content):
    headers = {'Content-type': 'application/json'}
    data = {}
    data["content"] = json.dumps(content)
    data["name"] = category
    data["slug"] = category
    return headers, data
