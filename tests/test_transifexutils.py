import responses
import requests
import pytest
import transifexutils
import requests
import base64
import json

from question import Question


@responses.activate
def test_uploadQuestions_creates_resource_when_category_does_not_exist(capsys):
    question = Question("Alright?", "Yes!", ["No"])
    # slight abuse of using a private method
    # but this reduces coupling slightly
    # since the test is not tied to how content is formatted before upload
    contentToUpload = transifexutils._transformQuestionsIntoContentFormat([
                                                                          question])

    category = "random-category"
    project = "random-project"
    authToken = "123"
    auth = ":".join([transifexutils.TRANSIFEX_API_USER, authToken])
    encodedAuth = str(base64.b64encode(auth.encode()), "ascii")

    getContentUrl = transifexutils.TRANSIFEX_CONTENT_PATH.format(
        project, category)
    postContentUrl = transifexutils.TRANSIFEX_RESOURCES_PATH.format(project)

    responses.add(responses.GET, getContentUrl,
                  status=requests.codes.not_found)
    responses.add(responses.POST, postContentUrl,
                  status=requests.codes.created)

    transifexutils.uploadQuestions([question], category, project, authToken)

    assert len(responses.calls) == 2
    # expect that the calls hit these urls
    assert responses.calls[0].request.url == getContentUrl
    assert responses.calls[1].request.url == postContentUrl
    # expect both requests to use our provided token
    assert responses.calls[0].request.headers['Authorization'].replace(
        "Basic ", "") == encodedAuth
    assert responses.calls[1].request.headers['Authorization'].replace(
        "Basic ", "") == encodedAuth
    # expect all content sent to consist of only our provided data
    contentSent = json.loads(json.loads(
        responses.calls[1].request.body)["content"])
    assert contentSent == contentToUpload


@responses.activate
def test_uploadQuestions_updates_existing_resource_when_category_exists_overriding_existing_entries():
    question = Question("Alright?", "Yes!", ["No"])
    contentToUpload = transifexutils._transformQuestionsIntoContentFormat([
                                                                          question])

    existingQuestion = Question("You sure?", "Yes!", ["No"])
    existingContent = transifexutils._transformQuestionsIntoContentFormat([
                                                                          existingQuestion])

    category = "random-category"
    project = "random-project"
    authToken = "123"
    auth = ":".join([transifexutils.TRANSIFEX_API_USER, authToken])
    encodedAuth = str(base64.b64encode(auth.encode()), "ascii")

    getContentUrl = transifexutils.TRANSIFEX_CONTENT_PATH.format(
        project, category)
    putContentUrl = transifexutils.TRANSIFEX_CONTENT_PATH.format(
        project, category)

    responses.add(responses.GET, getContentUrl,
                  json={"content": json.dumps(existingContent)},
                  status=requests.codes.ok)
    responses.add(responses.PUT, putContentUrl,
                  status=requests.codes.ok)

    transifexutils.uploadQuestions([question], category, project, authToken)

    # update existingContent with data to be uploaded
    existingContent.update(contentToUpload)

    assert len(responses.calls) == 2
    # expect that the calls hit these urls
    assert responses.calls[0].request.url == getContentUrl
    assert responses.calls[1].request.url == putContentUrl
    # expect both requests to use our provided token
    assert responses.calls[0].request.headers['Authorization'].replace(
        "Basic ", "") == encodedAuth
    assert responses.calls[1].request.headers['Authorization'].replace(
        "Basic ", "") == encodedAuth
    # expect all content sent to consist of our new data as well as what
    # the mocked response for existing data contained
    contentSent = json.loads(json.loads(
        responses.calls[1].request.body)["content"])
    assert contentSent == existingContent


@responses.activate
def test_uploadQuestions_raises_exception_when_resource_creation_fails():
    question = Question("Alright?", "Yes!", ["No"])
    contentToUpload = transifexutils._transformQuestionsIntoContentFormat([
                                                                          question])

    category = "random-category"
    project = "random-project"
    authToken = "123"

    getContentUrl = transifexutils.TRANSIFEX_CONTENT_PATH.format(
        project, category)
    postContentUrl = transifexutils.TRANSIFEX_RESOURCES_PATH.format(project)

    responses.add(responses.GET, getContentUrl,
                  status=requests.codes.not_found)
    responses.add(responses.POST, postContentUrl,
                  status=requests.codes.bad_request)
    with pytest.raises(Exception):
        transifexutils.uploadQuestions(
            [question], category, project, authToken)


@responses.activate
def test_uploadQuestions_raises_exception_when_resource_update_fails():
    question = Question("Alright?", "Yes!", ["No"])
    contentToUpload = transifexutils._transformQuestionsIntoContentFormat([
                                                                          question])

    existingQuestion = Question("You sure?", "Yes!", ["No"])
    existingContent = transifexutils._transformQuestionsIntoContentFormat([
                                                                          existingQuestion])

    category = "random-category"
    project = "random-project"
    authToken = "123"

    getContentUrl = transifexutils.TRANSIFEX_CONTENT_PATH.format(
        project, category)
    putContentUrl = transifexutils.TRANSIFEX_CONTENT_PATH.format(
        project, category)

    responses.add(responses.GET, getContentUrl,
                  json={"content": json.dumps(existingContent)},
                  status=requests.codes.ok)
    responses.add(responses.PUT, putContentUrl,
                  status=requests.codes.bad_request)

    with pytest.raises(Exception):
        transifexutils.uploadQuestions(
            [question], category, project, authToken)

    # with capsys.disabled():
    #     # print(dir(responses.calls[1].request))
    #     print(type(contentToUpload))
    #     print(type(contentSent))
