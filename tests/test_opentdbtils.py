import responses
import pytest
from opentdbutils import *
import requests
from question import Question

@responses.activate
def test_getAllCategories_returns_retrieved_categories(capsys):
    id1 = "9"
    name1 = "General Knowledge"
    id2 = "10"
    name2 = "Entertainment: Books"
    expectedRes = {name1: id1, name2: id2}
    responses.add(responses.GET, OPENTDB_CATEGORY_API_PATH,
                  json={'trivia_categories': [
                      {'id': id1, 'name': name1}, {'id': id2, 'name': name2}]},
                  status=requests.codes.ok)
    categories = getAllCategories()
    assert categories == expectedRes


@responses.activate
def test_getAllCategories_raises_exception_on_opentdb_404():
    responses.add(responses.GET, OPENTDB_CATEGORY_API_PATH,
                  json={'error': 'not found'}, status=requests.codes.not_found)
    with pytest.raises(Exception):
        getAllCategories()


@responses.activate
def test_getAllCategories_raises_exception_on_unexpected_opentdb_response():
    responses.add(responses.GET, OPENTDB_CATEGORY_API_PATH,
                  json={'trivial_category': []}, status=requests.codes.ok)
    with pytest.raises(Exception):
        getAllCategories()


@responses.activate
def test_getQuestionsForCategory_returns_retrieved_questions():
    numOfQuestions = 2
    category = "Anything"
    question1 = Question("True?", "True!", ["Nah."])
    question2 = Question("When I say jump, you say?", "How high.", ['No.'])
    expectedQuestions = [question1, question2]

    mockJson = {'results': [
        {'question': question1.question, 'correct_answer': question1.correctAnswer,
            'incorrect_answers': question1.incorrectAnswers},
        {'question': question2.question,
            'correct_answer': question2.correctAnswer, 'incorrect_answers': question2.incorrectAnswers}
    ]
    }
    responses.add(responses.GET,
                  OPENTDB_API_PATH+"?"+OPENTDB_QUESTION_NUMBER_PARAM + "=" +
                  str(numOfQuestions)+"&"+OPENTDB_CATEGORY_PARAM+"="+category,
                  json=mockJson,
                  status=requests.codes.ok)
    questions = getQuestionsForCategory(category, numOfQuestions)
    assert questions == expectedQuestions


@responses.activate
def test_getQuestionsForCategory_raises_exception_on_opentdb_404():
    numOfQuestions = 2
    category = "Anything"
    responses.add(responses.GET,
                  OPENTDB_API_PATH+"?"+OPENTDB_QUESTION_NUMBER_PARAM + "=" +
                  str(numOfQuestions)+"&"+OPENTDB_CATEGORY_PARAM+"="+category,
                  json={'error': 'not found'}, status=requests.codes.not_found)
    with pytest.raises(Exception):
        getQuestionsForCategory(category, numOfQuestions)


@responses.activate
def test_getQuestionsForCategory_raises_exception_on_unexpected_opentdb_response():
    numOfQuestions = 2
    category = "Anything"
    responses.add(responses.GET,
                  OPENTDB_API_PATH+"?"+OPENTDB_QUESTION_NUMBER_PARAM + "=" +
                  str(numOfQuestions)+"&"+OPENTDB_CATEGORY_PARAM+"="+category,
                  json={'whatever': 'here'},
                  status=requests.codes.ok)
    with pytest.raises(Exception):
        getQuestionsForCategory(category, numOfQuestions)
