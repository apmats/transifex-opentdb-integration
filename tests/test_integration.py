import pytest
import opentdbutils
import transifexutils
import integration
from unittest.mock import patch

def getACoupleOfCategories(): 
    return {"Art": "5", "History" : "7"}

@pytest.fixture
def patchedCategoryRetrieval(monkeypatch):
    monkeypatch.setattr(opentdbutils, 'getAllCategories', getACoupleOfCategories)

@patch("opentdbutils.getQuestionsForCategory")
@patch("transifexutils.uploadQuestions")
def test_fetchAndUploadQuestions_retrieves_and_sends_data_for_all_requested_categories(mock_getQuestionsForCategory, mock_uploadQuestions, patchedCategoryRetrieval):
    categoryList = ["Art", "History"]
    integration.fetchAndUploadQuestions(categoryList, 2, "random", "random")
    # expect these to be called for the work to be done
    assert mock_getQuestionsForCategory.called
    assert mock_uploadQuestions.called
    # expect one call for each category
    assert mock_getQuestionsForCategory.call_count == len(categoryList)
    assert mock_uploadQuestions.call_count == len(categoryList)
    # we could even check that the calls had the expected arguments,
    # but that's excessive and these tests are already heavily coupled

@patch("opentdbutils.getQuestionsForCategory")
@patch("transifexutils.uploadQuestions")
def test_fetchAndUploadQuestions_raises_exception_when_passed_unretrievable_category(mock_getQuestionsForCategory, mock_uploadQuestions, patchedCategoryRetrieval):
    # Mocks for the 2 static functions are only present here in case the test happens due to a bug
    #  to NOT raise an exception, in order to avoid calls to the actual remote services.
    # We simply expect an exception here due to the patched category retrieval
    with pytest.raises(Exception):
        integration.fetchAndUploadQuestions(["Politics"], 2, "random", "random")
