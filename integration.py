import transifexutils
import opentdbutils



def fetchAndUploadQuestions(categories, questionsPerCategory, authToken, project):
    categoryNameToId = opentdbutils.getAllCategories()
    enabledCategories = []
    try:
        for category in categories:
            enabledCategories.append(categoryNameToId[category])
    except:
        raise Exception("One of the categories passed as an argument isn't supported, the list of supported categories is: " +
                        ", ".join(categoryNameToId.keys()))

    for category in enabledCategories:
        questions = opentdbutils.getQuestionsForCategory(
            category, questionsPerCategory)
        transifexutils.uploadQuestions(questions, category, project, authToken)
