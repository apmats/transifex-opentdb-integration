# Open Trivia DB to Transifex integration

A take home assignment, part of the Transifex interview process.

The gist of this project is to provide an automated way to pull data from the Open Trivia DB and after tranforming it in a certain way, push it to a transifex project. 
Transifex allows you to organize your content in separate "resources" per project, so each category of questions (eg. History) are pushed in a separate resource with their ID being the integer ID of the category.


# Project structure

We provide the following modules:

* opentdbutils

Contains a few methods to retrieve category and question data relevant to our usages from the Open Trivia DB API. 
Also contains a few constants (urls and parameters used for the above purposes).

* transifexutils

Contains one publically accessible method that allows a caller to push some data corresponding to some questions to a transifex project.
Also contains a few constants (urls and parameters used for the above purposes).

* question

A simple dto for our 2 other modules to interact, in order to reduce coupling between them

* integration

A very simply module that is responsible for appropriately calling the methods exposed by the 2 main implementation modules.

* integrationcli

A command line utility, designed to simply parse arguments and forward them to the above module.
Designed as a demo/test for all the functionality.

An example use would be:

```bash
python3 integrationcli.py -c History -c Art -p project-name -n 2 -t token-here
```

Additionally, some tests are found under the /test directory. 

You can run these by simply running:

```bash
python3 -m pytest
```

# Added notes, what's missing and what could be done better

There could always be more tests. In some cases only the happy path is tested. 
It didn't help that this was done during an election weekend with limited time, and I spent a lot of time getting the hang of modules, imports, testing facilities etc.

We could also set up an end to end, integration smoke test that basically does what a user of our project would do from the command line: run everything with an actual token and existing transifex project and verify that the actual transifex service has the new data populated.

We're handling some tests with monkey patching, which is okay for this scope, but it could get out of hand in a larger project and dependency injection could be a more appropriate way to handle things.

Because a lot of our functions have no output and instead change the state of a (remote) system, a lot of our tests end up being "ugly" tests where we check that the expected calls or requests were made.

A lot of boilerplate code in tests, could probably be cleaned up by moving some of the common setup logic to a common place.

I don't write Python code too much, so a lot of things that could be done more elegantly are not done so.
Some best practices specific to the language aren't followed due to temporary ignorance.

I also didn't have time to investigate what's the most appropriate style to document your public APIs.
In a production ready project, I'd provide better documentation for the publically exposed methods.

While wrapping up all the work for this I realized there's an insidious bug.
Because of handling both the data to be sent as well as the data received from an existing resource from Transifex as dicts, and simply doing a dict update to update content before resending, an issue becomes apparent. 
If for a specific question we have retrieved in the past, the number of answers was reduced between calls to the Open Trivia DB API, this approach would fail because any non-overriden answers would still remain in the dict. 
If this is something we want to guard ourselves from, then some smarter update logic is required.




